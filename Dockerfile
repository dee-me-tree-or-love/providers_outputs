FROM python:3.7-alpine
COPY . /app
WORKDIR /app
ENV  PYTHONPATH=/app:/usr/local/lib/python3/dist-packages
RUN pip install -r requirements.txt
CMD ["python3", "provout/main.py"]