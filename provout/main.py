from provout.providers import fibonacci_provider
from provout.output import stdout_writer, text_file_writer


def get_provider():
    return fibonacci_provider


def get_output_writers():
    return [stdout_writer, text_file_writer]


FIBONACCI_TARGET = 25


def main():
    data_container = get_provider().provide(FIBONACCI_TARGET)
    for writer in get_output_writers():
        writer.output(data_container)

    print("done")


if __name__ == "__main__":
    main()



def sum_two(a, b):
    return a + b

assert sum_two(2, 2) == 4