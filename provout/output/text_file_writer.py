from provout.transfer.standard_transfer_object import StandardTransferObject

STANDARD_FILE_NAME = "output.txt"


def output(data_container: StandardTransferObject, **meta) -> None:
    output_file_name = meta.get('output_file', STANDARD_FILE_NAME)
    with open(output_file_name, "w") as write_file:
        write_file.write(str(data_container.data_dict))
