# Providers Outputs

Example of a small python project with separation of presentation and logic
layers.  

## Organisation

- `provout` - main source code directory of the project
    - `main.py` - functional composition managing the execution of the task
    - `output` - package with the output writers
    - `providers` - package with the data providers
    - `transfer` - package with the standard transfer objects

## Execution

Build the docker image with the source code:
```bash
$ docker build . -t <whatever tag you like>
```  

Execute the build image as a container:
```bash
$ docker run --rm -it -v `pwd`:/app <whatever tag you like>
```  

## Run tests

Make sure all the requirements are installed:
```bash
$ pip3 install -r requirements.tst.txt

# if not installed application requirements
$ pip3 install -r requirements.txt
```  

Run tests:
```bash
$ nosetests tests_u/
```  